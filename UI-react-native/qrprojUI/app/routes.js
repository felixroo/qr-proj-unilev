import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';

import Splash from './components/splashscreen/splashScreen';
import Login from './components/login/login';
import Home from './components/home/home';
import Individual from './components/leaderboard/individual';
import Region from './components/leaderboard/region';


export default class Routes extends Component{
    render(){
        return(
            <Router>
                <Stack key="root" hideNavBar={true}>
                    {/* <Scene key="splash" component={Splash} title="Splash Screen"/> */}
                    {/* <Scene key="login" component={Login} title="Login"/> */}
                    <Scene key="home" component={Home} title="Home"/>
                    <Scene key="individual" component={Individual} title="Individual"/>
                    <Scene key="region" component={Region} title="Region"/>
                </Stack>
            </Router>
            )
     }
}