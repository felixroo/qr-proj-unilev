import React, { Component } from 'react';
import {StatusBar, StyleSheet, ImageBackground} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class Splash extends Component {
    render() {

        setTimeout(() => {
            Actions.login()
         }, 3000)

        return (
            <ImageBackground source={require('../../images/SplashScreen.jpg')} style={styles.bgImg}>
                <StatusBar hidden={true} />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    bgImg:{
        alignItems: 'center',
        justifyContent: 'center',
        flex:1
    },
});