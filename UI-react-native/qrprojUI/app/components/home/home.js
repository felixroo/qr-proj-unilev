import React, { Component } from 'react';
import {
    ImageBackground,
    StyleSheet,
    StatusBar,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';

import {SafeAreaView} from 'react-navigation';
import { Col, Row, Grid } from "react-native-easy-grid";
import {Actions} from 'react-native-router-flux';

export default class Home extends Component {

    leader(){
        Actions.individual()
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#EE4036'}}>
                <ImageBackground source={require('../../images/bg_home.jpg')} style={styles.bgImg}>
                    <StatusBar hidden={true} />
                        <View style={styles.header}>
                            <Grid>
                                <Row style={styles.headerRow}>
                                    <Col style={styles.testBoxName}>
                                        <View style={styles.headerNameBox}>
                                            <Text style={styles.headerName1}>
                                                Hi, 
                                            </Text>
                                            <Text style={styles.headerName2}>
                                                Yudithia Anggraini
                                            </Text>
                                        </View>
                                    </Col>
                                    <Col style={styles.testBoxMid}>
                                        <Image
                                            style={styles.logoHome}
                                            source={require('../../images/logo-event.png')}
                                        />
                                    </Col>
                                    <Col style={styles.testBoxRegion}>
                                        <View style={styles.headerRegionBox}>
                                            <Text style={styles.headerRegionTitle}>
                                                Region:
                                            </Text>
                                            <Text style={styles.headerRegion}>
                                                HO SUPPORT
                                            </Text>
                                        </View>
                                    </Col>
                                    <Col style={styles.testBoxLogout}>
                                        <TouchableOpacity 
                                            style={styles.logoutBtnBox}
                                        >
                                            <Text style={styles.logoutBtn}>
                                                Logout
                                            </Text>
                                        </TouchableOpacity>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.container}>
                            <View style={styles.pointBox}>
                                <Grid>
                                    <Row>
                                        <Col style={styles.pointContent1}>
                                            <View style={styles.individual}>
                                                <Text style={styles.pointInd}>
                                                    5
                                                </Text>
                                                <Text style={styles.pts}>
                                                    pts
                                                </Text>
                                            </View>
                                            <Text style={styles.pointContentTxt}>
                                                Individual
                                            </Text>
                                        </Col>
                                        <Col style={styles.pointContent2}>
                                            <View style={styles.individual}>
                                                <Text style={styles.pointReg}>
                                                    10
                                                </Text>
                                                <Text style={styles.pts}>
                                                    pts
                                                </Text>
                                            </View>
                                            <Text style={styles.pointContentTxt}>
                                                Region
                                            </Text>
                                        </Col>
                                    </Row>
                                </Grid>
                            </View>
                            <View style={styles.banner}>
                            </View>
                            <View style={styles.iconTitle}>
                                <Text style={styles.title1}>
                                    EXPLORE
                                </Text>
                                <Text style={styles.title2}>
                                    Check out all our features
                                </Text>
                            </View>
                            <View style={styles.icon}>
                                <Grid>
                                    <Row>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_instruction.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_QR.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <TouchableOpacity
                                            onPress={this.leader}
                                            >
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_leader.png')}
                                            />
                                            </TouchableOpacity>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_quiz.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_log.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_info.png')}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_todo.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_Questions.png')}
                                            />
                                        </Col>
                                        <Col style={styles.colIcon}>
                                            <Image 
                                            style={styles.iconImage}
                                            source={require('../../images/btn_FAQ.png')}
                                            />
                                        </Col>
                                    </Row>
                                </Grid>
                            </View>
                        </View>
                </ImageBackground> 
            </SafeAreaView> 
        );
    }
}

const styles = StyleSheet.create({
    bgImg:{
        alignItems: 'center',
        justifyContent: 'center',
        flex:1
    },
    icon:{
        marginTop: 10,
        width: 290,
        height: 220,
        justifyContent: 'center',
        marginBottom: 30
    },
    iconContent:{
        textAlign: 'center'
    },
    iconImage:{
        width: 90,
        height: 70
    },
    colIcon:{
        // borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    iconTitle:{
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title1:{
        fontWeight:'600'
    },
    title2:{
        fontSize: 10,
        fontWeight: '200'
    },
    banner:{
        marginTop: 5,
        borderWidth: 1,
        borderColor: '#D3D3D3',
        backgroundColor: '#FFFFFF',
        width: 290,
        height: 150
    },
    pointBox:{
        width: 290,
        height: 50,
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#D3D3D3',
        borderRadius: 3,
        paddingTop: 5
    },
    pointContent1:{
        // borderWidth: 1,
        borderRightWidth: 1,
        borderColor: 'rgba(236, 236, 236, 1)'
    },
    pointContent2:{
        // borderWidth: 1,
        borderLeftWidth: 1,
        borderColor: 'rgba(236, 236, 236, 1)'
    },
    pointContentTxt:{
        textAlign: 'center',
        justifyContent: 'center',
        fontSize:9
    },
    logoHome:{
        width: 60,
        height: 58,
    },
    header:{
        width: '100%',
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        flex: 1,
        // borderWidth: 1
    },
    testBoxName:{
        alignItems: 'center',
        justifyContent: 'center',
        width: '40%',
        height: 58,
    },
    headerNameBox:{
        flexDirection: 'row'
    },
    headerName1:{
        textAlign: 'center',
        fontSize: 10,
        justifyContent: 'flex-start'
    },
    headerName2:{
        textAlign: 'center',
        fontSize: 10,
        color: '#EE4036',
        justifyContent: 'flex-end'
    },
    testBoxMid:{
        alignItems: 'center',
        // justifyContent: 'center',
        width: '20%',
    },
    pointInd:{
        fontSize: 23,
        fontWeight: '600',
        color: '#099CDA',
        textAlign: 'center'
    },
    pointReg:{
        fontSize: 23,
        fontWeight: '600',
        color: '#EE4036',
        textAlign: 'center'
    },
    individual:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    pts:{
        fontSize:10
    },
    container:{
        // borderWidth: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        width: '100%',
        height: '90%'
    },
    headerRow:{
        width: '100%',
        height: 58
    },
    headerRegionBox:{
        alignItems: 'center',
        justifyContent: 'center',
        height: 58
    },
    headerRegionTitle:{
        fontSize: 10,
        fontWeight: '600'
    },
    headerRegion:{
        fontSize: 8,
        fontWeight: '100'
    },
    logoutBtnBox:{
        marginTop: 14,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center',
        height: 28,
        borderLeftWidth: 1,
        borderColor: 'rgba(236, 236, 236, 1)'
    },
    logoutBtn:{
        fontSize: 10,
        textAlign: 'center',
        color: '#EE4036'
    }
});