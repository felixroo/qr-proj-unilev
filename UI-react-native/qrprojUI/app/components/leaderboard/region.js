import React, { Component } from 'react';
import {
    StatusBar, 
    StyleSheet, 
    TouchableOpacity,
    View,
    Text,
    ImageBackground} from 'react-native';

import {Actions} from 'react-native-router-flux';
import {SafeAreaView} from 'react-navigation';
import { Col, Row, Grid } from "react-native-easy-grid";

export default class Region extends Component {

    home(){
        Actions.home()
    }

    ind(){
        Actions.individual()
    }

    reg(){
        Actions.region()
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
                <ImageBackground source={require('../../images/leaderboard-region.jpg')} style={styles.bgImg}>
                    <StatusBar hidden={true} />
                    <TouchableOpacity onPress={this.home} style={styles.backBtn}></TouchableOpacity>
                    <View style={{width: '100%', height: 50, marginTop: 5, justifyContent: 'center', flexDirection: 'row', alignItems: 'center'}}>
                        <TouchableOpacity onPress={this.ind} style={styles.indBtn}></TouchableOpacity>
                        <TouchableOpacity onPress={this.reg} style={styles.regBtn}></TouchableOpacity>
                    </View>
                    <View style={styles.leaderboardContainer}>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoardRank1}>
                                        <Text style={styles.number}>
                                            1
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3Rank1}>
                                        <Text style={styles.colBoard2Point1}>
                                            60
                                        </Text>
                                        <Text style={styles.pts1}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoardRank2}>
                                        <Text style={styles.number}>
                                            2
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3Rank2}>
                                        <Text style={styles.colBoard2Point2}>
                                            60
                                        </Text>
                                        <Text style={styles.pts2}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoardRank3}>
                                        <Text style={styles.number}>
                                            3
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3Rank3}>
                                        <Text style={styles.colBoard2Point3}>
                                            60
                                        </Text>
                                        <Text style={styles.pts3}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoard1}>
                                        <Text style={styles.number}>
                                            4
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3}>
                                        <Text style={styles.colBoard2Point}>
                                            60
                                        </Text>
                                        <Text style={styles.pts}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoard1}>
                                        <Text style={styles.number}>
                                            5
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3}>
                                        <Text style={styles.colBoard2Point}>
                                            60
                                        </Text>
                                        <Text style={styles.pts}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoard1}>
                                        <Text style={styles.number}>
                                            6
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3}>
                                        <Text style={styles.colBoard2Point}>
                                            60
                                        </Text>
                                        <Text style={styles.pts}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoard1}>
                                        <Text style={styles.number}>
                                            7
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3}>
                                        <Text style={styles.colBoard2Point}>
                                            60
                                        </Text>
                                        <Text style={styles.pts}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                        <View style={styles.individual1}>
                            <Grid>
                                <Row style={styles.rowBoard}>
                                    <Col style={styles.colBoard1}>
                                        <Text style={styles.number}>
                                            8
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard2}>
                                        <Text style={styles.colBoard2Name}>
                                            William Shelby
                                        </Text>
                                    </Col>
                                    <Col style={styles.colBoard3}>
                                        <Text style={styles.colBoard2Point}>
                                            60
                                        </Text>
                                        <Text style={styles.pts}>
                                            pts
                                        </Text>
                                    </Col>
                                </Row>
                            </Grid>
                        </View>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    bgImg:{
        flex:1
    },
    backBtn:{
        width: 50,
        height: '6.5%',
        // borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    indBtn:{
        width: '50%',
        height: 40,
        // borderWidth: 1,
        flex: 1
    },
    regBtn:{
        width: '50%',
        height: 40,
        // borderWidth: 1,
        flex: 1
    },
    leaderboardContainer:{
        alignItems: 'center',
        width: '100%',
        height: '70%',
        paddingTop: 30,
        // borderWidth: 1
    },
    individual1:{
        width: 300,
        height: 40
    },
    rowBoard:{
        // borderWidth: 1,
        marginBottom: 5,
        marginRight: 30,
        width: '100%'
    },
    colBoardRank1:{
        borderRadius: 50,
        width:'11.5%',
        backgroundColor: '#099CDA',
    },
    colBoardRank2:{
        borderRadius: 50,
        width:'11.5%',
        backgroundColor: '#EE4036'
    },
    colBoardRank3:{
        borderRadius: 50,
        width:'11.5%',
        backgroundColor: '#F7A41B'
    },
    colBoard1:{
        borderRadius: 50,
        width:'11.5%',
        backgroundColor: '#a8a8a8'
    },
    colBoard2:{
        justifyContent: 'center',
        paddingLeft: 10,
        borderBottomWidth: 0.3,
        borderBottomColor: '#d3d3d3'
    },
    colBoard3:{
        paddingTop: 10,
        justifyContent: 'center',
        width: '13%',
        borderBottomWidth: 0.3,
        borderBottomColor: '#d3d3d3',
        flexDirection: 'row'
    },
    colBoard3Rank1:{
        paddingTop: 10,
        justifyContent: 'center',
        width: '15%',
        borderBottomWidth: 0.3,
        borderBottomColor: '#d3d3d3',
        flexDirection: 'row'
    },
    colBoard3Rank2:{
        paddingTop: 10,
        justifyContent: 'center',
        width: '14.5%',
        borderBottomWidth: 0.3,
        borderBottomColor: '#d3d3d3',
        flexDirection: 'row'
    },
    colBoard3Rank3:{
        paddingTop: 10,
        justifyContent: 'center',
        width: '14%',
        borderBottomWidth: 0.3,
        borderBottomColor: '#d3d3d3',
        flexDirection: 'row'
    },
    colBoard2Point1:{
        fontSize: 18,
        justifyContent: 'flex-end',
        color: '#099CDA',
        flex: 1
    },
    colBoard2Point2:{
        fontSize: 16,
        justifyContent: 'flex-end',
        color: '#EE4036'
    },
    colBoard2Point3:{
        fontSize: 14,
        justifyContent: 'flex-end',
        color: '#F7A41B'
    },
    colBoard2Point:{
        fontSize: 12,
        justifyContent: 'flex-end'
    },
    number:{
        paddingTop: 6,
        paddingRight: 1,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#FFFFFF'
    },
    colBoard2Name:{
        fontSize: 12,
        fontWeight: '400'
    },
    colBoard2Reg:{
        fontSize: 10,
        fontWeight: '200'
    },
    pts:{
        fontSize: 11,
        flex: 1
    },
    pts1:{
        paddingTop: 3,
        fontSize: 11,
        flex: 1,
        color: '#099CDA'
    },
    pts2:{
        paddingTop: 2,
        fontSize: 11,
        flex: 1,
        color: '#EE4036'
    },
    pts3:{
        paddingTop: 1,
        fontSize: 11,
        flex: 1,
        color: '#F7A41B'
    }
});