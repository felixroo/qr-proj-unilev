import React, { Component } from 'react';
import {StatusBar, StyleSheet, View, Image, Text, KeyboardAvoidingView, ImageBackground, TouchableOpacity, Button} from 'react-native';
import LoginForm from './loginForm'
import Modal from 'react-native-modalbox';
import {Actions} from 'react-native-router-flux';

export default class Login extends Component {

    constructor(props){
        super(props)
        this.state={
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3
        }
    }

    login(){
        Actions.login()
    }

    render() {
        return (
            <ImageBackground source={require('../../images/Header_login.jpg')} style={styles.bgImg}>
                <StatusBar hidden={true} />
                <KeyboardAvoidingView behavior="padding">
                    <View style={styles.inner}>
                        <LoginForm />
                        <View style={styles.signupTextCont}>
                            <Text style={styles.signupText}>Don't have an account yet?</Text>
                            <TouchableOpacity onPress={() => this.refs.modal3.open()}><Text style={styles.signupBtn}> Contact Us</Text></TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
                <Modal style={[styles.modal, styles.modal3]} position={"center"} ref={"modal3"} isDisabled={this.state.isDisabled}>
                    <Text style={[styles.ContactUsText, styles.headText]}>Any problem with your account?</Text>
                    <Text style={styles.ContactUsText}>Contact us via email</Text>
                    <Text style={[styles.ContactUsText, styles.footText]}>askquestion@gmail.com</Text>
                    <TouchableOpacity style={styles.okBox} onPress={() => this.refs.modal3.close()}>
                            <Text style={styles.okBoxText}>
                                OK
                            </Text>
                    </TouchableOpacity>
                </Modal>
            </ImageBackground>  
        );
    }
}

const styles = StyleSheet.create({
    bgImg:{
        alignItems: 'center',
        justifyContent: 'center',
        flex:1
    },
    inner:{
        marginTop: 425,
        width: 290,
        height: 300
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#FFFFFF'
    },
    logoContainer:{
        marginTop: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title:{
        color: 'white',
        marginTop: 10,
        width: 170,
        textAlign: 'center',
        opacity: 0.9
    },
    signupText:{
        color: 'black',
        fontSize: 13
    },
    signupBtn:{
        color: '#099CDA',
        fontSize:13,
        fontWeight: '500'
    },
    signupTextCont:{
        flexGrow:1,
        textAlign: 'center',
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    modal3: {
        height: 280,
        width: 280,
        borderRadius: 10
    },
    headText:{
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 30
    },
    footText:{
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 30
    },
    ContactUsText:{
        marginBottom: 20
    },
    okBox:{
        backgroundColor: '#099CDA',
        height: 40,
        width: 100,
        borderRadius:10,
        justifyContent: 'center'
    },
    okBoxText:{
        color: '#FFFFFF',
        textAlign: 'center'
    }
});