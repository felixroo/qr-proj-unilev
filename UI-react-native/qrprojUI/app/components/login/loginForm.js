import React, { Component } from 'react';
import {StyleSheet, View, TextInput, TouchableOpacity, Text, StatusBar} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class LoginForm extends Component {

    signup(){
        Actions.register()
    }

    home(){
        Actions.home()
    }

    constructor(props){
        super(props)
        this.state={
            userEmail:'',
            userPass:'',
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3
        }
    }

    userLogin = () =>{

        const {userEmail,userPass} = this.state;

        fetch('http://192.168.32.2:80/qrcodeproject/login.php', {
            method: 'POST',
            header:{
                'Accept' : 'application/json',
                'Content-type' : 'application/json'
            },
            body:JSON.stringify({
                user_email: userEmail,
                user_password: userPass,
            })
        })
        .then((response) => response.json())
            .then((responseJson) =>{
                if(responseJson == "Login Success"){
                    this.home();
                }else{
                    alert("Please Input Email and Password First");
                }
            })
            .catch((error) =>{
                console.error(error);
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="light-content"        
                />
                <TextInput 
                    placeholder="Email"
                    placeholderTextColor="#6D6D6D"
                    returnKeyType="next"
                    onSubmitEditing={() => this.passwordInput.focus()}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                    onChangeText= { userEmail => this.setState({userEmail}) }
                    style={styles.input}
                />
                <TextInput 
                    placeholder="Password"
                    placeholderTextColor="#6D6D6D"
                    returnKeyType="go"
                    secureTextEntry
                    style={styles.input}
                    onChangeText= { userPass => this.setState({userPass}) }
                    ref={(input) => this.passwordInput = input}
                />  
                <TouchableOpacity onPress={this.userLogin} style={styles.buttonContainer}>
                    <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop:10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10
    },
    input: {
        height: 38,
        backgroundColor: '#E4E4E4',
        marginBottom: 15,
        color: '#FFF',
        paddingHorizontal: 10,
    },
    buttonContainer:{
        backgroundColor: '#EE4036',
        paddingVertical: 10,
        marginBottom: 10
    },
    buttonText:{
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: '700'
    },
    signupText:{
        color: 'black',
        fontSize: 13
    },
    signupBtn:{
        color: '#099CDA',
        fontSize:13,
        fontWeight: '500'
    },
    signupTextCont:{
        flexGrow:1,
        textAlign: 'center',
        paddingVertical: 16,
        flexDirection: 'row',
        justifyContent: 'center'
    }
});